package k8s_service

import (
	"context"

	v1 "k8s.io/api/core/v1"
	v1beta1 "k8s.io/api/networking/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/watch"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

type Api struct {
	clientSet *kubernetes.Clientset
	config *rest.Config
	filePath string
}



func NewAPI(filePath string) (Api, error) {
	config, err := clientcmd.BuildConfigFromFlags("", filePath)
	if err != nil {
		panic(err.Error())
	}
	client, err := kubernetes.NewForConfig(config)

	if err != nil {
		panic(err.Error())
	}

	return Api{clientSet: client, filePath: filePath}, nil

}

func buildConfig(context, filePath string) (*rest.Config, error) {
	return clientcmd.NewNonInteractiveDeferredLoadingClientConfig(
		&clientcmd.ClientConfigLoadingRules{ExplicitPath: filePath},
		&clientcmd.ConfigOverrides{
			CurrentContext: context,
		}).ClientConfig()
}

func (api *Api) SwitchContext(context string) error {
	config, err := buildConfig(context, api.filePath)
	if err != nil {
		return err
	}

	client, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil
	}

	api.clientSet = client
	api.config = config

	return nil
}

func (api *Api) GetPos(namespace string) (*v1.PodList, error) {
	return api.clientSet.CoreV1().Pods(namespace).List(context.TODO(), metav1.ListOptions{})
}
func (api *Api) GetPo(namespace, name string) (*v1.Pod, error) {
	return api.clientSet.CoreV1().Pods(namespace).Get(context.TODO(), name, metav1.GetOptions{})
}

func (api Api) GetIngress(namspace string) (*v1beta1.IngressList, error) {
	return api.clientSet.NetworkingV1beta1().Ingresses(namspace).List(context.TODO(), metav1.ListOptions{})
}
func (api Api) Watcher(namespace string, opt metav1.ListOptions) (watch.Interface, error) {
	return api.clientSet.CoreV1().Pods(namespace).Watch(context.TODO(), opt)
}