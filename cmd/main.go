package main

import (
	"fmt"
	"github.com/sorushsaghari/space/internal/k8s_service"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/watch"
	"log"
)

func main() {
	api, err := k8s_service.NewAPI("/home/sorush/.kube/config")
	if err != nil {
		panic(err)
	}
	watcher, err := api.Watcher("mse-dev", metav1.ListOptions{LabelSelector: "app notin (redis,stolon), release!=pg1"})
	if err != nil {
		panic(err.Error())
	}
	// event := make(chan watch.Event)
	for {
		event := <-watcher.ResultChan()
		getStatus(event)
	}
	// time.Sleep(5 * time.Second)
}

func getStatus(event watch.Event) {

	fmt.Println(event.Type)
	p, ok := event.Object.(*v1.Pod)
	if !ok {
		log.Fatal("unexpected type")
	}
	fmt.Println(p.Name, p.Status.Phase)
	fmt.Println()

	// fmt.Println(event.Object.GetObjectKind().GroupVersionKind().ToAPIVersionAndKind())
}
